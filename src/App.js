import React, { Component } from 'react';
import { SubmissionError } from 'redux-form';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PersonList from './PersonList';
import PersonForm from './PersonForm';

import * as formActions from './actions/form';
import * as personActions from './actions/person';
import { loadPersons, createPerson, updatePerson } from './request';

class App extends Component {
  constructor(props) {
    super(props);
		this.initialValues = {
			name: "",
			phone: "",
		};
    this.state = {
      isAdd: false,
    	editedPerson: null,
		};

    this.onDelete = (deletedPerson) => () => {
      this.props.Person.destroy(deletedPerson.id);
    };

    this.onAdd = () => {
			const values = {...this.initialValues};
      this.props.Form.initForm("person", {
        name: "",
        phone: "",
      });
      this.setState({ isAdd: true, editedPerson: null, values }); 
    };

    this.onCancelAdd = () => {
      this.setState({ isAdd: false }); 
    };

    this.onCommitAdd = (values) => {
      const promise = createPerson(values);
      return promise.then(() => {
        alert('data successfully created');

        const promise = loadPersons();
        this.props.Person.loadAllPending();
        promise.then(response => {
          const persons = response.data;
          this.props.Person.loadAllFulfilled(persons);
          this.onCancelAdd();
        });
      });
    };

	  this.onEdit = (editedPerson) => () => {
      this.props.Form.initForm("person", editedPerson);
			const values = {...editedPerson};
			this.setState({ editedPerson, isAdd: false, values });	
		};

		this.onCancelEdit = () => {
			this.setState({ editedPerson: null });
		};

		this.onCommitEdit = (values) => {
      const promise = updatePerson(values);
      return promise.then(() => {
        alert("data successfully updated");

        const promise = loadPersons();
        this.props.Person.loadAllPending();
        promise.then(response => {
          const persons = response.data;
          this.props.Person.loadAllFulfilled(persons);
          this.onCancelEdit();
        });
      });
		};
  }

  componentDidMount() {
    const promise = loadPersons();
    this.props.Person.loadAllPending();
    promise.then(response => {
      const persons = response.data;
      this.props.Person.loadAllFulfilled(persons);
    });
  }

  render() {
    const { isAdd, editedPerson } = this.state;
    const { persons, isLoading } = this.props;

    return (<div className='container'>
      {isAdd && <PersonForm onCancel={this.onCancelAdd}
        onSubmit={this.onCommitAdd}/>}
      {editedPerson && <PersonForm onCancel={this.onCancelEdit}
        onSubmit={this.onCommitEdit}/>}
      <button onClick={this.onAdd}>Add Person</button>
      <PersonList isLoading={isLoading} persons={persons}
        onDelete={this.onDelete}
				onEdit={this.onEdit}/>
    </div>);
  }
}

function mapStateToProps(state) {
  const { persons, isLoading } = state.persons;
  return {
    persons, isLoading
  };
}

function mapDispatchToProps(dispatch) {
  return {
    Form: bindActionCreators(formActions, dispatch),
    Person: bindActionCreators(personActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
