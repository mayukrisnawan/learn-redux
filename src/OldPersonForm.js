import React from 'react';

class PersonForm extends React.Component {
  render() {
    const { onCancel, onSubmit, onChange, values } = this.props;

    return (<form style={{ marginBottom: 20 }} onSubmit={onSubmit}>
      <label>Name</label>
      {" "}
      <input type='text' name='name' onChange={onChange("name")} value={values.name}/>
      <br/>
      <label>Phone</label>
      {" "}
      <input type='text' name='phone' onChange={onChange("phone")} value={values.phone}/>
      <br/>
      <button onClick={onCancel}>Cancel</button>
      <button type='submit'>Save</button>
    </form>);
  }
}

export default PersonForm;
