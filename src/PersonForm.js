import React from 'react';
import { reduxForm, Field } from 'redux-form';
import TextField from './TextField';

class PersonForm extends React.Component {
  render() {
    const { handleSubmit, submitting, onCancel } = this.props;
    const { meta } = this.props;

    return (<form onSubmit={handleSubmit}>
      <label>Name</label>
      {" "}
      <Field type='text' name='name' component={TextField}/>
      <br/>
      <label>Phone</label>
      {" "}
      <Field type='text' name='phone' component={TextField}/>
      <br/>
      <button onClick={onCancel}>Cancel</button>
      <button type='submit'>{submitting ? "Saving..." : "Save"}</button>
    </form>);
  }
}

export default reduxForm({
  form: "person",
})(PersonForm);
