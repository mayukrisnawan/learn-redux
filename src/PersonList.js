import React from 'react';

class PersonList extends React.Component {
  render() {
    const { isLoading, persons=[] } = this.props;
    const { onDelete, onEdit } = this.props;

    if (isLoading) {
      return <div>Loading...</div>;
    }

    return (<table style={{ width: "100%" }}>
      <thead>
        <tr>
          <th style={{ textAlign: "left" }}>No</th>
          <th style={{ textAlign: "left" }}>Name</th>
          <th style={{ textAlign: "left" }}>Phone</th>
          <th style={{ textAlign: "left" }}>Option</th>
        </tr>
      </thead>
      <tbody>
        {persons.map((person, index) => {
          return (<tr key={person.id}>
            <td>{index+1}</td>
            <td>{person.name}</td>
            <td>{person.phone}</td>
            <td>
              <button onClick={onEdit(person)}>Edit</button>
              <button onClick={onDelete(person)}>Delete</button>
            </td>
          </tr>);
        })}
      </tbody>
    </table>);
  }
}

export default PersonList;
