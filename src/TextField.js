import React from 'react';

class TextField extends React.Component {
  render() {
    const { input, label, type } = this.props;
    const { meta: { touched, error, warning } } = this.props;

    return (<div>
      <label>{label}</label>
      <div>
        <input {...input} placeholder={label} type={type} />
        {touched &&
          ((error && <span style={{ color: "red" }}>{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    </div>);
  }
}

export default TextField;
