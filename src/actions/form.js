import { initialize } from 'redux-form';

export function initForm(form, values) {
  return initialize(form, values);
}
