export function loadAllPending() {
  return {
    type: "PERSON/LOAD_ALL_PENDING",
  };
}

export function loadAllFulfilled(persons) {
  return {
    type: "PERSON/LOAD_ALL_FULFILLED",
    payload: persons,
  };
}

export function destroy(id) {
  return {
    type: "PERSON/DESTROY",
    payload: id,
  };
}

export function create(values) {
  return {
    type: "PERSON/CREATE",
    payload: values,
  };
}

export function update(id, values) {
	return {
		type: "PERSON/UPDATE",
		payload: {id, values},
	};
}
