import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import persons from './persons';

const reducer = combineReducers({
  persons,
  form,
});

export default reducer;
