const initialState = {
  persons: [],
  isLoading: false,
};

export default function personsReducer(state=initialState, action) {
  if (action.type === "PERSON/LOAD_ALL_PENDING") {
    return {...state, isLoading: true};
  }
  
  if (action.type === "PERSON/LOAD_ALL_FULFILLED") {
    const persons = action.payload;
    return {...state, isLoading: false, persons};
  }
  
  if (action.type === "PERSON/DESTROY"){
    let persons = [...state.persons];
    for (let index in persons) {
      const person = persons[index];
      if (action.payload === person.id) {
        persons.splice(index, 1);
        return {...state, isLoading: false, persons};
      }
    }
  }

  if (action.type === "PERSON/CREATE") {
    let persons = [...state.persons];
    let id = null;
    for (let index in persons){
      const person = persons[index];
      if (id === null || person.id > id) {
        id = person.id;
      }
    }
		id += 1;
    const person = {
      ...action.payload,
      id,
    };
    persons.push(person);
		return {...state, persons};
  }

	if (action.type === "PERSON/UPDATE") {
		let persons = [...state.persons];
		for (let index in persons) {
			const person = persons[index];
			if (person.id === action.payload.id) {
				persons[index] = action.payload.values;
				break;
			}
		}
		return {...state, persons};
	}
  return state;
}
