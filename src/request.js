import axios from 'axios';
import { SubmissionError } from 'redux-form';

export function api(options={}) {
  const request = axios.create({
    baseURL: "http://localhost:4000",
  });
  request.interceptors.response.use(response => {
    return response;
  }, error => {
    if (error.response && error.response.status == 422) {
      let errors = error.response.data;
      throw new SubmissionError(errors);
    }
    return Promise.reject(error);
  });
  return request;
}

export function loadPersons() {
  return api().get("persons");
}

export function createPerson(person) {
  return api().post("persons", { person });
}

export function updatePerson(person) {
  return api().put(`persons/${person.id}`, { person });
}
